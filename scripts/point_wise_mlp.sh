#!/usr/bin/env bash

python code/optim_knapsack.py --name worked-ok --use_rand_e --dim_e 16 --n_prods 10000 \
--n_epoch 1000 --total_tolerance 500 --lr 0.001 \
--copt_beta 10 --fc1_dims 64 64 128  --fc2_dims 128 16

python code/optim_knapsack.py --name rand-e-share-weights --use_rand_e --dim_e 32 \
--n_prods 1000 --n_epoch 1000 --total_tolerance 500 --lr 0.001 --copt_beta 10 \
--fc1_dims 64 128 512 --fc2_dims 128 64 \
--bsn_pass

python code/optim_knapsack.py --name all-share-weights --use_rand_e --dim_e 16 --use_val_cost --use_budget \
--n_prods 1000 --n_epoch 1000 --total_tolerance 500 --lr 0.001 --copt_beta 10 \
--fc1_dims 64 128 512 --fc2_dims 128 64 \
--bsn_pass