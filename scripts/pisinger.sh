#!/usr/bin/env bash

python code/optim_knapsack.py --name pisinger-t11-r1000 \
--dataroot data/Pisinger/small-coefficients/type11_r=1000.csv \
--use_rand_e --dim_e 16 --use_shared_e --dim_shared_e 64 \
--n_epoch 1000 --total_tolerance 500 --lr 0.001 \
--copt_beta 10 --fc1_dims 128 64 64