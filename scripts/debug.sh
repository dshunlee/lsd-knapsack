#!/usr/bin/env bash

python code/optim_knapsack.py --name worked-ok --n_prods 1000000 \
--n_epoch 1000 --total_tolerance 500 --lr 0.001 \
--use_rand_e --dim_e 16  --use_shared_e --dim_shared_e 64 \
--copt_beta 10 --fc1_dims 128 64 64