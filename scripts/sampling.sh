#!/usr/bin/env bash

python3 code/sampling_pointnet.py --data_root data/modelnet40_ply_hdf5_2048/train_files.txt --model pointnet \
--dataset_name modelnet40 --batch_size 1 --print_freq 1 --n_epoch 200 \
--fixed_pc_id 0 --sampling_perc 0.5 --use_rand_e \
# --sampling_fc_dims 512 128 1