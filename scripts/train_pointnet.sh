#!/usr/bin/env bash

python3 code/train_pointnet.py --data_root /work/pcc/pc2mesh/data/modelnet40_ply_hdf5_2048/train_files.txt --n_epoch 300 \
--model pointnet --dataset_name modelnet40 --batch_size 32 --print_freq 500