#!/usr/bin/env bash

python code/optim_knapsack.py --name rand-e --use_rand_e --dim_e 32 \
--n_prods 1000 --n_epoch 200 --lr 0.0001 --copt_beta 10 \
--fc1_dims 64 128 512 --not_share_weights \
--bsn_sampling

#!/usr/bin/env bash
python code/optim_knapsack.py --name val-cost --use_val_cost --use_budget \
--n_prods 1000 --n_epoch 200 --lr 0.0001 --copt_beta 10 \
--fc1_dims 64 128 512 --not_share_weights \
--bsn_sampling