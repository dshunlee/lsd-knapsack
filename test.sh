# You need the learning rate of 1. If not, it takes long time to optimize.
python code/optim_knapsack.py --name pisinger-t3_r=10^7 --dataroot data/Pisinger/large-coefficients/type3_r=10^7.csv --use_rand_e --dim_e 1 --n_epoch 10000 --total_tolerance 10000 --lr 100 --copt_beta 10 --fc1_dims 1
