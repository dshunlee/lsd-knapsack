# Large-scale Deep Knapsack (LSD-Knapsack)
Large-scale knapsack optimization using deep neural networks.

Folder structure:

    --root/
        --data/ # place to store data, local, do not commit
        --scripts/ # place to store shell scripts for data preparation, training, and optimisation.
        --code/
            --util/ # utility functions
            --options/ # general options for training and testing
            --models/ # neural network model related code
                --base_model # all models must inherite from this base model, the file name should end with _model       
            --data/ # codes to prepare/load data
            --baseline/ # codes for baseline methods
            
To create your model, options for particular model can be configed by overwrite the function `def modify_commandline_options(parser, is_train=True)`. Check `knapsack_model` for reference.

For each model, the final loss function is `self.loss_cost`. If you want to print out other costs, name the costs as `self.losss_[loss_name]`, and append a string `loss_name` to `self.loss_names`.

For other operations `self.operation_x` you want to check, append a string`'operation_x'`, them into `self.visual_ops`.

For baseline methods that do not need tensorflow, put them into the baseline folder, and the interface of the function should be `def func_name(value, cost, budget)` where `value` and `cost` are numpy array.

## Data preparation

### Knapsack Problem
Data were generated randomly. 

  + `--min_val [float]`, minimum value, default=0.0001
  + `--max_val [float]`, maximum value, default=1.0
  + `--min_cost [float]`, minimum cost, default=0.0001
  + `--max_cost [float]`, maximum cost, default=1.0
  + `--min_budget [float]`, minimum budget, default=1.0
  + `--max_budget [float]`, maximum budget, default=5.0
  + `--rand_value [str]`, randum number method for value, default="uniform"
  + `--rand_cost [str]`, randum number method for cost, default="uniform"
  + `--rand_budget [str]`, randum number method for budget, default="uniform"
  
### PointNet

run:

    python code/data/pc_provider.py

## Optimization of KnapsackProblem

Optimization has following options.

For inputs:

  + `--use_val_cost [switch]`, use value cost and budget as input.
  + `--use_rand_e [switch]`, use random vectors as input.
  + `--dim_e`, dimension of variable e, default 4.

For FC layers:

  + `--fc1_dims [list of int]`,number of features of fc block1, default=[64, 64, 64]
  + `--fc2_dims [list of int]`,number of features of fc block2, default=[64, 64, 64], 
  this is only useful when share weights.
  + `--activ [str]`,  activiation layer, default=NOne
  + `--norm [str]`,normalization layer, default=NOne
  
For Binary Stochastic Neurons:

  + `--bsn_estimator [str]`, default='st', binary stocastic neuron estimator
  + `--bsn_pass [switch]`, pass through for st estimator, if `True` use `slope annealing`
  + `--bsn_sampling [switch]`, sampling the prediction
  + `--bsn_anneal_policy [str]`, default='exp', slope annealing policy
  + `--bsn_anneal_rate [float]`, default=1.01, slope annealing rate
  + `--bsn_anneal_step [int]`, default=1, slope annealing step
  
For optimizaiton:

  + `--over_budget_penalty [float]`, default=1.0, initial beta, \beta_0
  + `--baseline_methods [list of str]`, default=['gr'], baseline methods to test.

The result is stored at log_loss.txt file. Greedy method is implemented for verification.


To check the demo, run:

      python optim_knapsack.py --use_rand_e --n_prods 1000 --n_epoch 200 \
      --lr 0.0001 --bsn_sampling --fc_nfeats 64 128 512 \
      --copt_beta 10 
      

## Point Cloud Sampling

### Train PointNet

Train pointnet using
 
    bash train_pointnet.sh

Resampling using

    bash resampling.sh