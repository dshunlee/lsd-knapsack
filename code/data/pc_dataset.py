
import tensorflow as tf
import numpy as np
import itertools
from .pc_provider import loadAllFromFiles, getDataFiles, get_sub_smpl_data, get_optimzd_data
import os


class PointCloudDataset:
    def __init__(self, data_root, dataset=None, batch_size=16, classes=None, n_points=2048,
                 shuffle=True, use_cov=False, augment=False,
                 fixed_pc_id=None):
        self.batch_size, self.data_root, self.dataset = batch_size, data_root, dataset
        self.n_batch, self.classes, self.n_pcs, self.n_points = None, classes, 0, n_points + 1
        self.shuffle, self.augment = shuffle, augment
        self.global_id, self.use_cov = 0, use_cov
        self.out_dim, self.pcs, self.labels = 3, None, None
        self.fixed_pc_id = fixed_pc_id
        np.random.seed(0)

    def create_dataset(self, instance_file=None, subsmpl_file=None, subsmpl_file_suffix=None):
        if self.dataset == "modelnet40":
            if os.path.splitext(self.data_root)[1] == '.txt':
                self._create_modelnet40_dataset_hdf5()
            else:
                self._create_modelnet40_dataset_pickle()
        elif ("subsample" in self.dataset) | ('optimized' in self.dataset):
            self._create_subsample_dataset(instance_file, subsmpl_file, subsmpl_file_suffix)
        else:
            self._create_torus_dataset()

        if self.classes is not None:
            sel_pcs, sel_lab = [], []
            for c in self.classes:
                sel_pcs += [self.pcs[self.labels == c]]
                sel_lab += [self.labels[self.labels == c]]

            self.pcs = np.concatenate(sel_pcs, 0)
            self.labels = np.concatenate(sel_lab, 0)
        else:
            self.classes = np.unique(self.labels)

        self.n_pcs, self.n_points = self.pcs.shape[0], self.pcs.shape[1] + 1
        self.n_batches = self.n_pcs // self.batch_size

        if self.fixed_pc_id is None:
            generator = self.generator  # if not self.augment else self.augmented_generator
        else:
            generator = self.fixed_generator

        return tf.data.Dataset.from_generator(generator=generator,
                                              output_types=(tf.float32, tf.int32),
                                              output_shapes=(tf.TensorShape([self.n_points, self.out_dim]),
                                                             tf.TensorShape([])))

    def generator(self):
        if self.shuffle:
            id = np.random.randint(self.n_pcs)
            yield np.vstack((self.pcs[id], np.zeros((1, 3), np.float32))), self.labels[id]
        else:
            # for i in itertools.count(1):
            for i in range(self.n_pcs):
                # id = i % self.n_pcs
                id = i
                # print('id {} / label {}'.format(id, self.labels[id]))
                yield np.vstack((self.pcs[id], np.zeros((1, 3), np.float32))), self.labels[id]

    def fixed_generator(self):
        yield np.vstack((self.pcs[self.fixed_pc_id], np.zeros((1, 3), np.float32))), self.labels[self.fixed_pc_id]

    def augmented_generator(self):
        if self.shuffle:
            id = np.random.randint(self.n_pcs)
            pc= self.pcs[id]
        else:
            for i in itertools.count(1):
                pc = self.pcs[i % self.n_pcs]

        s = 1 + np.random.rand(1, 3) * 0.4 - 0.2
        pc = pc * s
        pc = pc / np.sqrt(np.max(np.sum(pc * pc, -1)))

        yield pc

    def _create_modelnet40_dataset_hdf5(self):
        """ for dataset downloaded from https://shapenet.cs.stanford.edu"""
        pcs, labels = loadAllFromFiles(self.data_root)
        self.pcs, self.labels = np.vstack(pcs), np.vstack(labels).flatten()

    def _create_modelnet40_dataset_pickle(self):
        """for dataset downloaded from FoldingNet"""
        data = np.load(self.data_root)
        self.pcs = data['data'].astype(np.float32)
        self.labels = data['label'].astype(np.float32)
        if 'cov' in data.keys() and self.use_cov:
            cov = data['cov'].astype(np.float32)
            self.pcs = np.hstack(self.pcs, cov)
            self.out_dim = 12

    def _create_torus_dataset(self):
        files = getDataFiles(self.data_root)
        data_root = os.path.dirname(self.data_root)
        pcls = []
        for fi in files:
            pci = np.loadtxt(os.path.join(data_root, fi))
            pci = pci / np.sqrt(np.max(np.sum(pci*pci, -1)))
            idx = np.arange(self.n_points) % pci.shape[0]
            pcls += [pci[idx, :]]
        self.pcs = np.stack(pcls, 0)
        self.labels = None

    def _create_subsample_dataset(self, instance_file, subsmpl_file, subsmpl_file_suffix):
        pcs, labels = None, None
        if self.dataset == "subsample":
            pcs, labels = get_sub_smpl_data(instance_file, self.n_points - 1, self.data_root,
                                            subsmpl_file, subsmpl_file_suffix)
        elif 'optimized' in self.dataset:
            pcs, labels = get_optimzd_data(instance_file, self.n_points - 1, self.data_root,
                                           subsmpl_file, subsmpl_file_suffix)
        self.pcs, self.labels = np.vstack([pcs]), np.vstack(labels).flatten()
        assert self.pcs.shape[0] == self.labels.shape[0]
        return
