import os
import h5py
from numpy import genfromtxt
import numpy as np


def loadAllFromFiles(filename):
    files = getDataFiles(filename)
    data, label = [], []
    for fi in files:
        di = loadDataFile(fi)
        data += [di[0]]
        label += [di[1]]
    return data, label


def load_h5(h5_filename):
    f = h5py.File(h5_filename, 'r')
    data = f['data'][:]
    label = f['label'][:]
    return data, label


def loadDataFile(filename):
    return load_h5(filename)


def getDataFiles(list_filename):
    return [line.rstrip() for line in open(list_filename)]


def get_sub_smpl_data(instance_file, point_num, data_root, subsmpl_file, subsmpl_file_suffix):
    _, labels = loadAllFromFiles(data_root)
    labels = np.vstack(labels).flatten()
    instances = [line.rstrip() for line in open(instance_file)]
    sampled_pcs = []
    sampled_labels = []

    for instance in instances:
        # print('instance: {}'.format(instance))
        if subsmpl_file_suffix != 'no':
            sub_smpl_file = subsmpl_file + '_ins_{}_{}.csv'.format(instance, subsmpl_file_suffix)
        else:
            sub_smpl_file = subsmpl_file + '_ins_{}.csv'.format(instance)
        pc = genfromtxt(sub_smpl_file, delimiter=',')
        # padding zero to maximum point num
        if pc.shape[0] < point_num:
            # print('padding zero')
            pc = np.vstack((pc, np.zeros((point_num - pc.shape[0], 3), np.float32)))
        # pc = np.zeros((point_num, 3))
        sampled_pcs.append(pc)
        sampled_labels.append(labels[int(instance)])
    return sampled_pcs, sampled_labels


def get_optimzd_data(instance_file, point_num, data_root, subsmpl_file, subsmpl_file_suffix):
    data, labels = loadAllFromFiles(data_root)
    labels = np.vstack(labels).flatten()
    data = np.vstack(data)
    # print('original data shape: {}'.format(data.shape))
    instances = [line.rstrip() for line in open(instance_file)]
    sampled_pcs = []
    sampled_labels = []
    saved_losses = []

    for instance in instances:
        sub_smpl_file = subsmpl_file + '_ins-{}_{}'.format(instance, subsmpl_file_suffix)
        opti_result = np.load(sub_smpl_file)
        pc_mask = np.reshape(opti_result['x'][0:point_num], (-1, 1))
        saved_loss = opti_result['v']
        # print('{} saved_pred/label: {}/{}'.format(instance, opti_result['best_pred'], opti_result['label']))
        pc = data[int(instance)]
        # print('point cloud shape for instance {}: {}'.format(instance, pc.shape))
        pc = np.multiply(pc_mask, pc)
        saved_losses.append(saved_loss)
        sampled_pcs.append(pc)
        sampled_labels.append(labels[int(instance)])
    return sampled_pcs, sampled_labels


def save_instance_label(DATA_DIR):
    _, label = loadAllFromFiles(os.path.join(DATA_DIR, 'modelnet40_ply_hdf5_2048/test_files.txt'))
    label = np.vstack(label).flatten()
    instances = [line.rstrip() for line in open('data/modelnet40_ply_hdf5_2048/org_pc_test/test_instance_small.txt')]
    with open(os.path.join(DATA_DIR, 'modelnet40_ply_hdf5_2048/test_label.txt'), 'w') as fout:
        for i in instances:
            fout.write('{}, {}\n'.format(i, label[int(i)]))


if __name__ == "__main__":
    DATA_DIR = 'data'
    if not os.path.exists(DATA_DIR):
        os.mkdir(DATA_DIR)
    if not os.path.exists(os.path.join(DATA_DIR, 'modelnet40_ply_hdf5_2048')):
        www = 'https://shapenet.cs.stanford.edu/media/modelnet40_ply_hdf5_2048.zip'
        zipfile = os.path.basename(www)
        os.system('wget --no-check-certificate %s; unzip %s' % (www, zipfile))
        os.system('mv %s %s' % (zipfile[:-4], DATA_DIR))
        os.system('rm %s' % (zipfile))

    # _, sample_labels = get_optimzd_data('data/modelnet40_ply_hdf5_2048/org_pc_test/good_instance.txt',
    #                                     2048,
    #                                     'data/modelnet40_ply_hdf5_2048/test_files.txt',
    #                                     'checkpoints/good_ckpt/test',
    #                                     'pect-0.05_msksize-0.9/optimal.npz')
    #
    # sample_labels = np.vstack(sample_labels).flatten()
    #
    # instances = [line.rstrip() for line in open('data/modelnet40_ply_hdf5_2048/org_pc_test/good_instance.txt')]
    _, label = loadAllFromFiles('data/modelnet40_ply_hdf5_2048/test_files.txt')
    label = np.vstack(label).flatten()
    with open(os.path.join(DATA_DIR, 'modelnet40_ply_hdf5_2048/org_pc_test/instances/all_instances_label.txt'), 'w') as fout:
        for i in range(label.shape[0]):
            fout.write('{}, {}\n'.format(i, label[i]))

    #
    #
    # with open(os.path.join(DATA_DIR, 'modelnet40_ply_hdf5_2048/sampled_instances_label.txt'), 'w') as fout:
    #     for i in range(len(instances)):
    #         fout.write('{}, {}\n'.format(instances[i], sample_labels[i]))



