from models.constrained_optim_model import ConstrainedOptimizationModel
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np


def test_adptive_beta(beta0, init_x, lr=0.1, method='gd'):
    """
    :param beta0: beta0 for adaptive beta
    :param init_x: initial x
    :param lr: learning rate
    :param method: ['gd'|'adam']
    """
    x = tf.get_variable("x", dtype=tf.float32, initializer=init_x)
    value = tf.reduce_sum(x * x)
    dx = x - tf.constant([1.0, 1.0])
    cost = tf.reduce_sum(dx*dx) - 1.0

    com = ConstrainedOptimizationModel(beta0, method="lagrange", bound_right=True, clip_right=True)

    loss = -value + com.adapt_budget_penalty(value, cost, x)

    gdir = tf.gradients(loss, x)[0]

    sess = tf.Session()
    if method == 'gd':
        train_ops = tf.train.GradientDescentOptimizer(lr).minimize(loss)
    elif method == 'adam':
        train_ops = tf.train.AdamOptimizer(lr).minimize(loss)

    sess.run(tf.global_variables_initializer())

    xs, gds = [], []
    for i in range(50):
        values = sess.run([x, value, cost, loss, [com.beta, com._left_bound, com._right_bound], gdir, train_ops])
        print('value: {:.03f}'.format(values[1]),
              'cost: {:.03f}'.format(values[2]),
              'loss: {:.03f}'.format(values[3]),
              'x:', values[0],
              'beta:', values[4])

        xs += [values[0]]
        gds += [values[5]]

    xs, gds = np.array(xs), np.array(gds)

    fig, ax = plt.subplots(1, 2, figsize=[12, 6], sharey=True)

    def draw_fig(ax, show_gradient, title):
        ax.set_aspect('equal', 'datalim')
        ax.add_patch(plt.Circle((1, 1), 1, alpha=0.5, color='grey'))
        ax.add_patch(plt.Circle((0.5, 0.5), np.sqrt(2)/2, alpha=0.5, color='b'))
        ax.scatter([1+np.sqrt(2.0)/2], [1+np.sqrt(2.0)/2], s=21, zorder=3, marker='o', c='r')
        ax.scatter(xs[-1:, 0], xs[-1:, 1], s=11, zorder=3, marker='*', c='b')
        ax.scatter(xs[:, 0], xs[:, 1], zorder=1, s=11)
        if not show_gradient:
            ax.quiver(xs[:-1, 0], xs[:-1, 1], xs[1:, 0]-xs[:-1, 0], xs[1:, 1]-xs[:-1, 1],
                      scale_units='xy', angles='xy', scale=1, zorder=2, alpha=0.7)
        else:
            ax.quiver(xs[:-1, 0], xs[:-1, 1], -gds[:-1, 0]*lr, -gds[:-1, 1]*lr,
                      scale_units='xy', angles='xy', scale=1, zorder=2, alpha=0.7)

        # plot gradient of objective function
        x = np.arange(-1, 2.5, 0.2)
        gx, gy = np.meshgrid(x, x)
        dis = 10*np.sqrt(gx*gx + gy*gy)
        nx, ny = gx/dis, gy/dis
        ax.quiver(gx.flatten(), gy.flatten(), nx, ny,
                  scale_units='xy', angles='xy', scale=1, zorder=0, alpha=0.3, color='g')
        # plot gradient of cost function
        x = np.arange(-1, 2.5, 0.2)
        gx, gy = np.meshgrid(x, x)
        dis = 10 * np.sqrt((gx-1) * (gx-1) + (gy-1) * (gy-1))
        nx, ny = -(gx-1) / dis, -(gy-1) / dis
        ax.quiver(gx.flatten(), gy.flatten(), nx, ny,
                  scale_units='xy', angles='xy', scale=1, zorder=0, alpha=0.3, color='r')

        ax.set_title(title)

    draw_fig(ax[0], show_gradient=True, title="true gradient at x")
    draw_fig(ax[1], show_gradient=False, title="actual updates at x")
    plt.suptitle('Green arrow: gradient increases objective; red arrow: gradient decreases cost. '
                 'Grey circle cost under budget; blue circle: $o\'b\'<0$.')
    plt.show()


test_adptive_beta(1.0, [-0.5, 0.5], 0.1, 'gd')