import tensorflow as tf


class ConstrainedOptimizationModel(object):
    def __init__(self, beta0, method="lagrange", bound_right=True, clip_right=True,
                 update_lr=False, lr=0.001, lr_decay=0.9, lr_adapt=False):
        self.beta0, self.method, self.bound_right, self.clip_right = beta0, method, bound_right, clip_right
        self.beta = tf.Variable(beta0, trainable=False, name="budget_penalty_beta")
        # learning rate not applied in previous experiments, effect not clear
        self.update_lr, self.lr, self.lr_decay, self.lr_adapt = update_lr, lr, lr_decay, lr_adapt

    def adapt_budget_penalty(self, objective, overbudget, variable):
        self.objective, self.overbudget, self.variable = objective, overbudget, variable
        self._left_bound, self._right_bound = tf.constant(0.0), tf.constant(0.0)

        if self.variable is None:
            self.variable = tf.trainable_variables()

        updates = self._update_beta()
        pen_overbudget = self._modify_penalty(updates)
        return pen_overbudget

    def _update_beta(self):
        # self._cal_gradient()
        # tfprint = tf.print("dfa", self._sum_df_df, self._sum_df_dg, self._sum_dg_dg,
        #                    self._sum_df_dg / (self._sum_dg_dg * self.overbudget), self._left_bound,
        #                    self._sum_df_df / (self._sum_df_dg * self.overbudget), self._right_bound)
        # if under budget, set beta to zero
        pen, lr, self._left_bound, self._right_bound = tf.cond(self.overbudget > 0, self._over_budget, self._set_2_zero)

        # tfprint = tf.print('beta', self.beta, 'pen:', pen, 'upper', self.overbudget * self.beta0 + self._left_bound,
        #                    'left: ', self._left_bound, 'right: ', self._right_bound)
        # with tf.control_dependencies([tfprint]):
        update_penalty = self.beta.assign(tf.nn.relu(pen))
        updates = [update_penalty]
        if self.update_lr:
            learning_rate = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, "learning_rate")[0]
            update_lr = learning_rate.assign(lr)
            updates += [update_lr]
        return updates

    def _cal_gradient(self):
        dfs = tf.gradients(self.objective, self.variable)
        dgs = tf.gradients(self.overbudget, self.variable)

        df = tf.concat([tf.reshape(df, (-1,)) for df in dfs], -1)
        dg = tf.concat([tf.reshape(dg, (-1,)) for dg in dgs], -1)

        self._sum_dg_dg = tf.reduce_sum(dg * dg)
        self._sum_df_dg = tf.reduce_sum(df * dg)
        self._sum_df_df = tf.reduce_sum(df * df)

        if self.method == "lagrange":
            self._left_bound = self._sum_df_dg / (self._sum_dg_dg * self.overbudget)
            self._right_bound = self._sum_df_df / (self._sum_df_dg * self.overbudget)
        else:
            self._left_bound = self._sum_df_dg / self._sum_dg_dg
            self._right_bound = self.sum_df_df / self._sum_df_dg

        #self.custom_beta = self.overbudget * self.beta0 + self._left_bound
        self.custom_beta = 1 / (self.lr * self._sum_dg_dg)
        #self.custom_beta = tf.minimum(1 / (self.lr * self._sum_dg_dg), self.overbudget * self.beta0 + self._left_bound)

    def _modify_penalty(self, updates):
        with tf.control_dependencies(updates):
            if self.method == "lagrange":
                loss_over_budget = tf.nn.relu(self.overbudget) ** 2 * self.beta / 2
            else:
                loss_over_budget = tf.nn.relu(self.overbudget) * self.beta
            return loss_over_budget

    def _set_2_zero(self):
        learning_rate = 0.0
        if self.update_lr:
            learning_rate = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, "learning_rate")[0]
        return tf.constant(0.0), learning_rate, tf.constant(0.0), tf.constant(0.0)

    def _over_budget(self):
        self._cal_gradient()

        # if the denominator of the left bound is negative or numerator of the left bound is zero, set beta to 0
        if self.bound_right:
            return tf.cond(tf.logical_or(tf.less(self._sum_df_dg, 0), tf.equal(self._sum_dg_dg, 0)),
                           self._set_2_zero, self._left_bounded)
        else:
            return tf.cond(tf.logical_or(tf.less(self._sum_df_dg, 0), tf.equal(self._sum_dg_dg, 0)),
                           self._set_2_zero, self._left_bounded_ignore_right)

    def _left_bounded(self):
        # the left bound is a positive number,  check right legal bounded or not
        pen = tf.cond(tf.logical_and(is_a_number(self._right_bound), self._right_bound > self._left_bound),
                      self._both_sides_bounded, self._right_not_bounded)
        lr = self._adapt_learning_rate()
        return pen, lr, self._left_bound, self._right_bound

    def _left_bounded_ignore_right(self):
        lr = self._adapt_learning_rate()
        return self.custom_beta, lr, self._left_bound, self._right_bound

    def _both_sides_bounded(self):
        if self.clip_right:
            return tf.minimum((self._left_bound + self._right_bound) / 2,
                              self.custom_beta)
        else:
            return (self._left_bound + self._right_bound) / 2

    def _right_not_bounded(self):
        if self.clip_right:
            return self.custom_beta
        else:
            return tf.cond(tf.less(self._right_bound, 0), lambda: self.custom_beta,
                           lambda: 2 * self._left_bound)  # right bound is inf (legal but not bounded)

    def _adapt_learning_rate(self):
        # following strategies are not tested, adjust them accordingly if needed
        learning_rate = 0.0
        if self.update_lr:
            learning_rate = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, "learning_rate")[0]
            if self.lr_decay == 0.0:
                learning_rate = self.lr * self.overbudget
            else:
                learning_rate = learning_rate * self.lr_decay

        return learning_rate


def is_a_number(x):
    return tf.logical_not(tf.logical_or(tf.is_nan(x), tf.is_inf(x)))