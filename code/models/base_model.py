
from collections import OrderedDict
import tensorflow as tf
import os
import numpy as np
import scipy.misc
import matplotlib.pyplot as plt

class BaseModel(object):
    # modify parser to add command line options,
    # and also change the default values if needed
    @staticmethod
    def modify_commandline_options(parser, is_train):
        return parser

    @staticmethod
    def get_mlp_model(input, dims, act_layer='relu', norm_layer=None, skip_last_act=True, training=True):
        for i in range(len(dims)):
            input = tf.keras.layers.Dense(dims[i], use_bias=False)(input)
            if norm_layer is not None:
                input = normalization_layer(norm_layer)(input, training=training)
            if i == len(dims)-1 and skip_last_act:
                pass
            elif act_layer is not None:
                input = activiation_layer(act_layer)(input)
        return input

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def name(self):
        return 'BaseModel'

    # initialize the network based on the options
    def initialize(self, opt, **kwargs):
        self.opt = opt
        self.visual_ops = []
        self.eval_ops = ['loss_cost']
        self.loss_names = ['cost']
        self.eval_stats = {}
        self.eval_stats['loss_cost'] = []
        self.loss_cost = 0.0  # loss
        self.losses = [self.loss_cost]
        self.exp_name = opt.name
        self.save_dir = os.path.join(opt.checkpoints_dir, opt.name)
        self.feed_dict = {}

        for k, v in kwargs.items():
            setattr(self, k, v)

    def get_costs_ops(self):
        cost_ops = []
        for cost in self.loss_names:
            cost_ops += [getattr(self, "loss_" + cost)]

        return cost_ops

    def get_vis_ops(self):
        vis_ops = []
        for vis in self.visual_ops:
            if hasattr(self, vis):
                vis_ops.append(getattr(self, vis))
        return vis_ops

    def custom_network(self, inputs, train_phase=True, **kwargs):
        pass

    def load_weights(self, sess, **kwargs):
        pass

    def get_save_varlist(self, var_list=None):
        return tf.global_variables()

    def init_stats(self):
        self.eval_stats['loss_cost'] = []

    def collect_stats(self, values):
        self.eval_stats['loss_cost'] += [values[0]]

    def display_stats(self, name=None):
        return {'avg_loss': np.mean(self.eval_stats['loss_cost'])}

    # setup the dataset
    def set_dataset(self, dataset):
        self.dataset = dataset

    # return visualization images. train.py will display these images, and save the images to a html
    def get_current_visuals(self):
        visual_ret = OrderedDict()
        for name in self.visual_ops:
            if isinstance(name+"_vis", str):
                visual_ret[name+"_vis"] = getattr(self, name+"_vis")
        return visual_ret

    def get_evaluation_ops(self):
        eval_ops = []
        for name in self.eval_ops:
            if isinstance(name, str):
                eval_ops += [getattr(self, name)]
        return eval_ops

    # return traning losses/errors. train.py will print out these errors as debugging information
    def get_current_losses(self):
        errors_ret = OrderedDict()
        for name in self.loss_names:
            if isinstance(name, str):
                # float(...) works for both scalar tensor and float number
                errors_ret[self.exp_name + '_' + name] = float(getattr(self, 'loss_' + name).eval())
        return errors_ret

    def update_learning_rate(self,**kwargs):
        learning_rate = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, "learning_rate")[0]
        return learning_rate

    # save models to the disk, need revision
    def save_networks(self, which_epoch):
       pass

    # compute visualization
    def compute_visuals(self, **kwargs):
        visop = {'col': 8, 'shape': None, 'interp':'bilinear', 'inverse': False}
        for k in visop:
            if k in kwargs.keys():
                visop[k] = kwargs[k]

        if 'vis' in kwargs.keys():
            visual = kwargs['vis']
            for i in range(len(self.visual_ops)):
                setattr(self, self.visual_ops[i] + "_vis",
                        self._batchtensor2grid(visual[i], visop['col'], visop['shape'], visop['interp'], visop['inverse']))

    def _batchtensor2grid(self, batch, col=8, shape=None, interp="bilinear", inverse=False):
        maxv = np.max(batch)
        if batch.ndim == 3:
            row = int(np.ceil(float(batch.shape[0])/col))
            N = row * col
            N0 = batch.shape[0]
            batch = np.vstack((batch, np.ones_like(np.expand_dims(batch[0], 0), dtype=np.float64).repeat(N-N0, 0)))
            batch = np.pad(np.array(batch), ((0, 0), (1, 1), (1, 1)), 'constant', constant_values=(maxv))
            batch = np.pad(np.array(batch), ((0, 0), (2, 2), (2, 2)), 'constant', constant_values=(0))
            batch = batch.reshape(row, col, batch[0].shape[0], batch[0].shape[1]).transpose(0, 2, 1, 3)
            batch = batch.reshape(row * batch[0].shape[0], -1)
        elif batch.ndim == 2:
            batch = np.pad(np.array(batch), ((2, 2), (2, 2)), 'constant', constant_values=(maxv))
            batch = np.pad(np.array(batch), ((5, 5), (5, 5)), 'constant', constant_values=(0))
        else:
            raise ValueError("The dimesion of visual ops is {}, but expected 2 or 3.".format(batch.ndim))

        batch = batch/maxv
        if inverse:
            batch = 1 - batch
        if shape is not None:
            batch = scipy.misc.imresize(batch, shape, interp)
        return batch

    def _batchtensor2fig(self, batch, col=8):
        maxv = np.max(batch)
        if batch.ndim == 2:
            pass
        elif batch.ndim == 3:
            row = int(np.ceil(float(batch.shape[0]) / col))
            N = row * col
            N0 = batch.shape[0]
            batch = np.vstack((batch, np.ones_like(np.expand_dims(batch[0], 0), dtype=np.float64).repeat(N - N0, 0)))
            batch = np.pad(np.array(batch), ((0, 0), (1, 1), (1, 1)), 'constant', constant_values=(maxv))
            batch = np.pad(np.array(batch), ((0, 0), (2, 2), (2, 2)), 'constant', constant_values=(0))
            batch = batch.reshape(row, col, batch[0].shape[0], batch[0].shape[1]).transpose(0, 2, 1, 3)
            batch = batch.reshape(row * batch[0].shape[0], -1)
        else:
            return None

        fig = plt.figure()
        fig.set_size_inches((6, 6))
        # Hide grid lines
        plt.grid(False)
        # Hide axes ticks
        plt.axis('off')
        # make axis range [-1, 1]
        plt.imshow(batch/maxv)
        fig.canvas.draw()
        batch_fig = np.array(fig.canvas.renderer._renderer)[..., :-1]
        # batch_pc.append(data)
        plt.cla()
        plt.clf()
        plt.close("all")
        return batch_fig


def activiation_layer(name=None):
    if name == 'relu':
        return tf.keras.layers.ReLU()
    return None


def normalization_layer(name=None):
    if name == 'batchnorm':
        return tf.keras.layers.BatchNormalization()
    return None