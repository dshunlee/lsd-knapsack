import tensorflow as tf


class BaseNet(tf.keras.layers.Layer):
    def __init__(self, nl_layer='relu', bn_layer=None, drop_rate=None, skip_last_nl=True, is_train=True, **kwargs):
        """
        :param nl_layer: string, none linear layer
        :param bn_layer: string, batchnorm layer
        :param skip_last_nl: boolean, skip last nonlinear layer or not
        :param is_train: boolean or tensor, is train or not
        :param kwargs:
        """
        super(BaseNet, self).__init__(**kwargs)
        self.nl_layer, self.bn_layer, self.dropout = None, None, None

        if nl_layer is not None:
            if nl_layer == 'relu':
                self.nl_layer = tf.nn.relu
            else:
                raise NotImplementedError("Nonelienar layer {} not implemented!".format(nl_layer))

        if bn_layer is not None:
            if bn_layer == 'batchnorm':
                self.bn_layer = tf.layers.batch_normalization
            else:
                raise NotImplementedError("Normalization layer {} not implemented!".format(bn_layer))

        if drop_rate is not None:
            self.dropout = tf.keras.layers.Dropout(rate=drop_rate)

        self.skip_last_act, self.is_train = skip_last_nl, is_train


class MLPNet(BaseNet):
    def __init__(self, kernels, name=None, **kwargs):
        super(MLPNet, self).__init__(**kwargs)
        self.kernels, self.layers = kernels, []
        self.prefix = name

    def call(self, input,  **kwargs):
        for k in range(len(self.kernels)):
            name = self.prefix+"-dense-{:02d}".format(k) if self.prefix is not None else None
            input = tf.layers.dense(input, self.kernels[k], name=name)
            self.layers += [input]

            if self.skip_last_act and k == len(self.kernels)-1:
                break

            # apply batch norm
            if self.bn_layer is not None:
                input = self.bn_layer(input, training=self.is_train)

            if self.nl_layer is not None:
                input = self.nl_layer(input)

            if self.dropout is not None:
                input = self.dropout(input, training=self.is_train)

        return input


class PointNet(BaseNet):
    def __init__(self, mlp_dims, fc_dims, **kwargs):
        super(PointNet, self).__init__(**kwargs)
        self.mlp_dims, self.fc_dims = mlp_dims, fc_dims
        self.mlp_net = MLPNet(kernels=self.mlp_dims, name="pointnet-mlp", **kwargs)
        self.fc_net = MLPNet(kernels=self.fc_dims, name='pointnet-fc', **kwargs)

    def call(self, input, **kwargs):
        pre_pool = self.mlp_net(input, **kwargs)
        self.post_mlp_x = pre_pool
        return self._pool_and_predict(pre_pool, **kwargs)

    def _pool_and_predict(self, pre_pool, **kwargs):
        if 'mask' in kwargs.keys():
            self.pre_pool = pre_pool * kwargs['mask']
        if "pooling" in kwargs.keys() and kwargs['pooling'].lower() == "ave":
            self.code = tf.reduce_mean(pre_pool, 1)
            #code = tf.reduce_mean(pre_pool, 1)
        else:
            self.code = tf.reduce_max(pre_pool, 1)
            #code = tf.reduce_max(pre_pool, 1)

        pred = self.fc_net(self.code)
        if 'return_code' in kwargs.keys():
            return pred, self.code
        else:
            return pred


class Conv2D(tf.keras.layers.Conv2D):
    def call(self, inputs, *args, **kwargs):
        return tf.keras.layers.Conv2D.call(self, inputs, *args, **kwargs)


class ConvNet(BaseNet):
    def __init__(self, filters, kernel_size=None, **kwargs):
        super(ConvNet, self).__init__(**kwargs)
        self.filters, self.kernel_size = filters, kernel_size
        if self.kernel_size is None:
            self.kernel_size = [1] *len(self.filters)

    def call(self, input, padding='same', **kwargs):
        for k in range(len(self.filters)):
            input = Conv2D(self.filters[k], self.kernel_size[k], padding=padding)(input)

            if self.skip_last_act and k == len(self.filters) - 1:
                break

            # apply batch norm
            if self.bn_layer is not None:
                input = self.bn_layer(input, training=self.is_train)

            if self.nl_layer is not None:
                input = self.nl_layer(input)

        return input


class FoldingNetSingle(ConvNet):
    def __init__(self, dims, kernel_size=None, **kwargs):
        super(FoldingNetSingle, self).__init__(dims, kernel_size, **kwargs)

    def call(self, input, **kwargs):
        """
        :param input: input B x N x Kin
        :param G: Graph B x N x G
        :param index: 5N
        :param sumG: row sum of G, B x N
        :return: B x N x Kout
        """
        return ConvNet.call(self, input, **kwargs)
