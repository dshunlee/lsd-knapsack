from sklearn.metrics import f1_score, recall_score, precision_score, accuracy_score, roc_auc_score
import os, time
import tensorflow as tf
import numpy as np


def train(opt, model, train_dataset, test_dataset=None, **kwargs):
    # handle constructions. Handle allows us to feed data from different dataset by providing a parameter in feed_dict
    handle = tf.placeholder(tf.string, shape=[])
    # used to distinguish train and inference stage for simultaneous training and testing
    train_phase = tf.placeholder(tf.bool, name="is_training")

    iterator = tf.data.Iterator.from_string_handle(handle, train_dataset.output_types,
                                                   train_dataset.output_shapes)
    next_batch = iterator.get_next()

    # create training/test data iterator
    training_iterator = train_dataset.make_one_shot_iterator()

    # modify isTrain option
    feed_dict = {train_phase: opt.isTrain}

    # initialize the models
    # model.initialization(opt, **kwargs)
    # add dropout
    drop_ratio = None
    if opt.drop_ratio is not None:
        drop_ratio = tf.placeholder_with_default(opt.drop_ratio, shape=())
        feed_dict[drop_ratio] = opt.drop_ratio

    global_step = tf.Variable(0, trainable=False, name='global_step')
    # set up annealing policy here
    annealing_slope = get_annealing_slope(opt, global_step=global_step)
    # define learning rate
    learning_rate = get_learning_rate(opt, global_step=global_step)

    train_var_list = model.custom_network(next_batch, train_phase=train_phase, dropout=drop_ratio,
                                          bsn_annealing_slope=annealing_slope)
    # get operators
    cost_ops = model.get_costs_ops()
    vis_ops = model.get_vis_ops()

    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    with tf.control_dependencies(update_ops):
        # create train operations

        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)

        train_op = optimizer.minimize(model.loss_cost, global_step=global_step, var_list=train_var_list)

    # config log settings
    saver = tf.train.Saver(model.get_save_varlist(), max_to_keep=opt.n_epoch)

    log_filename = os.path.join(model.save_dir, "log_loss.txt")
    print_options(log_filename, opt)

    # start training
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        # get_mem_summary(sess, train_op, handle, train_phase, train_dataset, models.save_dir, drop_ratio)
        sess.run(tf.local_variables_initializer())
        sess.run(tf.global_variables_initializer())
        #model.load_weights(sess)
        # get number of batches in one epoch
        nbatches = opt.n_batches
        # feed training data to handle
        training_handle = sess.run(training_iterator.string_handle())
        # training_handle, test_handle = sess.run([training_iterator.string_handle(), test_iterator.string_handle()])
        feed_dict[handle] = training_handle
        # best_result [loss, influence, over_budget, time]
        niter, total_steps, total_time, best_result, best_freq = 0, 0, 0, [0, 0, 0, 0], None

        for epoch in range(opt.n_epoch):
            #  add training code
            ave_loss = None
            for i in range(nbatches):
                total_steps += opt.batch_size
                start_time = time.time()
                values = sess.run([cost_ops, vis_ops, train_op], feed_dict=feed_dict)
                losses = np.array(values[0])
                ave_loss = losses if ave_loss is None else (i * ave_loss + losses) / (i + 1)
                if total_steps % opt.print_freq == 0:
                    message = "epoch {}, iter {}, {} : batch {}, ave {}\n"\
                        .format(epoch, i, model.loss_names, losses, ave_loss)
                    with open(log_filename, 'a') as logfile:
                        logfile.write(message)
                    print(message.replace('\n', ', iter time {} s'.format(time.time()-start_time)))

                # if i > 150:
                #     feed_dict[train_phase] = False
                #     model.check_result(sess, feed_dict, prefix='{:02d}-{:03d}-'.format(epoch, i))
                #     feed_dict[train_phase] = True

            if (epoch+1) % opt.save_epoch_freq == 0:
                saver.save(sess, os.path.join(model.save_dir, "{:05d}.ckpt".format(epoch+1)))
                # check results on training set
                # feed_dict[train_phase] = False
                # model.check_result(sess, feed_dict, prefix='{:05d}-'.format(epoch+1))
                # feed_dict[train_phase] = True

        saver.save(sess, os.path.join(model.save_dir, "latest.ckpt"))
        # check results on training set
        # feed_dict[train_phase] = False
        # model.check_result(sess, feed_dict, prefix='{:05d}-'.format(epoch+1))
        # feed_dict[train_phase] = True


def inference(opt, model, dataset, **kwargs):
    iterator = dataset.make_one_shot_iterator()
    next_batch = iterator.get_next()
    # initialize the models
    model.initialization(opt, **kwargs)
    model.custom_network(next_batch, train_phase=False)

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    save_dir = os.path.join(model.save_dir, opt.which_epoch)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    train_phase = tf.placeholder(tf.bool, name="is_training")
    print('opt.isTrain: {}'.format(opt.isTrain))
    feed_dict = {train_phase: opt.isTrain}

    with tf.Session(config=config) as sess:
        sess.run(tf.local_variables_initializer())
        sess.run(tf.global_variables_initializer())
        labels, preds, prob, code_word, accuracy, loss_vec = [], [], [], [], [], []
        raw_input, logit, post_mlp_x = [], [], []
        if model.load_weights(sess):
            for i in range(opt.n_batches):
                results = model.check_result(sess, feed_dict=feed_dict, prefix='{}/{:05d}-'.format(opt.which_epoch, i+1))
                if results is not None:
                    labels.append(results[0])
                    preds.append(np.argmax(results[1], 1))
                    prob.append(np.max(results[1], 1))
                    code_word.append(results[2])
                    loss_vec.append(results[3][2])
                    print('loss {}'.format(results[3][2]))
                    print('prob {}'.format(results[1]))
        else:
            import shutil
            shutil.rmtree(model.save_dir)
        labels = np.vstack(labels).flatten()
        preds = np.vstack(preds).flatten()
        code_word = np.vstack(code_word)
        loss_vec = np.vstack(loss_vec)
        print('code word shape: {}'.format(code_word.shape))
        print('loss vec dimension: {}'.format(loss_vec.shape))
        for i in range(preds.shape[0]):
            print('preds/label: {}/{}'.format(preds[i], labels[i]))
        print('Accuracy: {:.3f}'.format(len(np.where(labels==preds)[0])/len(labels)))
        # accuracy, w_f1, micro_f1, macro_f1 = cal_eval_metrics(y_true=labels, y_pred=preds)
        # print(accuracy, w_f1, micro_f1, macro_f1)
        # filename = 'data/eval_results.txt'
        # if not os.path.isfile(filename):
        #     with open(filename, 'w') as fout:
        #         fout.write('experiment_name\taccuracy\tw_f1\tmicro_f1\tmacro_f1\n')
        #         fout.write('{}\t{}\t{}\t{}\t{}\n'.
        #                    format(opt.name, accuracy, w_f1, micro_f1, macro_f1))
        # else:
        #     with open(filename, 'a') as fout:
        #         fout.write('{}\t{}\t{}\t{}\t{}\n'.
        #                    format(opt.name, accuracy, w_f1, micro_f1, macro_f1))
        #
        # redo_idx = np.where(labels != preds)[0]
        # print('# of redo instances: {}'.format(len(redo_idx)))
        # from numpy import genfromtxt
        # all_instance = genfromtxt('data/modelnet40_ply_hdf5_2048/org_pc_test/sample_instance.txt')
        # redo_instances = all_instance[redo_idx]
        # good_idx = np.where(labels == preds)[0]
        # good_instances = all_instance[good_idx]
        # with open('data/modelnet40_ply_hdf5_2048/org_pc_test/instances/redo_instance_0.9.txt',
        #           'w') as fout:
        #     for item in list(redo_instances):
        #         fout.write('{}\n'.format(int(item)))
        #
        # with open('data/modelnet40_ply_hdf5_2048/org_pc_test/instances/good_instance_0.9.txt',
        #           'w') as fout:
        #     for item in list(good_instances):
        #         fout.write('{}\n'.format(int(item)))
        # print(save_dir)
        # np.savez(os.path.join(save_dir, "preds_result.npz"),
        #          labels=labels, preds=preds, prob=np.vstack(prob).flatten(),
        #          code_word=code_word, loss_vec=loss_vec)


def get_annealing_slope(opt, **kwargs):
    annealing_slope = None
    if opt.bsn_estimator == 'st' and opt.bsn_pass:
        annealing_slope = tf.Variable(1.0, trainable=False, name="annealing_slope",
                                      collections=[tf.GraphKeys.UPDATE_OPS])
    if opt.bsn_anneal_policy == 'exp':
        annealing_slope = tf.train.exponential_decay(1.0, kwargs['global_step'],
                                                     opt.bsn_anneal_step,
                                                     opt.bsn_anneal_rate, staircase=False)
    else:
        pass

    return annealing_slope


def get_learning_rate(opt, **kwargs):
    if opt.lr_decay_policy == 'exp':
        learning_rate = tf.train.exponential_decay(opt.lr, kwargs['global_step'],
                                                   100, 0.96, staircase=False)
    elif opt.lr_decay_policy == 'custom':
        learning_rate = tf.Variable(opt.lr, dtype=tf.float32, trainable=False, name="learning_rate")
    else:
        return opt.lr
    return learning_rate


def print_options(log_name, opt):
    message = ''
    message += '\n\n================== Options ==================\n'
    for k, v in sorted(vars(opt).items()):
        comment = ''
        message += '{:>25}: {:<30}{}\n'.format(str(k), str(v), comment)
    message += '----------------- End -------------------'

    # save to the disk
    with open(log_name, 'a') as opt_file:
        opt_file.write(message)
        opt_file.write('\n')


def cal_eval_metrics(y_true, y_pred):

    print('y true shape {}, y pred shape {}'.format(y_true.shape, y_pred.shape))

    w_f1 = f1_score(y_true, y_pred, average="weighted")
    macro_f1 = f1_score(y_true, y_pred, average="macro")
    accuracy = accuracy_score(y_true, y_pred)
    micro_f1 = f1_score(y_true, y_pred, average="micro")
    return accuracy, w_f1, micro_f1, macro_f1