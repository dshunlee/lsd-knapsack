#/bin/bash

# TF1.4
/usr/bin/nvcc tf_approxmatch_g.cu -o tf_approxmatch_g.cu.o -c -O2 -DGOOGLE_CUDA=1 -x cu -Xcompiler -fPIC
g++ -std=c++11 tf_approxmatch.cpp tf_approxmatch_g.cu.o -o tf_approxmatch_so.so -shared -fPIC -I /home/lid/anaconda3/envs/venv-tensorflow-2.0-alpha-python3.6/lib/python3.6/site-packages/tensorflow/include -I /usr/local/cuda-10.0/include -I /home/lid/anaconda3/envs/venv-tensorflow-2.0-alpha-python3.6/lib/python3.6/site-packages/tensorflow/include/external/nsync/public -lcudart -L /usr/lib/x86_64-linux-gnu/lib64/ -L/home/lid/anaconda3/envs/venv-tensorflow-2.0-alpha-python3.6/lib/python3.6/site-packages/tensorflow -ltensorflow_framework -O2 -D_GLIBCXX_USE_CXX11_ABI=0
