TF_PATH=/home/lid/anaconda3/envs/cuda10python3.6/lib/python3.6/site-packages/tensorflow
/usr/bin/nvcc tf_nndistance_g.cu -o tf_nndistance_g.cu.o -c -O2 -DGOOGLE_CUDA=1 -x cu -Xcompiler -fPIC
g++ -std=c++11 tf_nndistance.cpp tf_nndistance_g.cu.o -o tf_nndistance_so.so -shared -fPIC -I $TF_PATH/include -I $TF_PATH/include/external/nsync/public -lcudart -L$TF_PATH -l:libtensorflow_framework.so.1 -O2 -D_GLIBCXX_USE_CXX11_ABI=0

