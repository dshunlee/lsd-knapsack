from .basic_nets import MLPNet, PointNet
from .base_model import BaseModel
import tensorflow as tf
import numpy as np
from .binary_stocastic_neurons import binary_stocastic_layer
from .constrained_optim_model import ConstrainedOptimizationModel
import os


class PointNetModel(BaseModel):
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        parser.add_argument('--classes', type=int, nargs='+', help='category ids used to train/test the model')
        parser.add_argument('--n_points', type=int, default=2048, help='pointnet fc dims')
        parser.add_argument('--pointnet_mlp_dims', type=int, nargs='+', default=[64, 64, 64, 128, 1024],
                            help='pointnet mlp dims')
        parser.add_argument('--pointnet_fc_dims', type=int, nargs='+', default=[512, 256, 40], help='pointnet fc dims')

        ###
        parser.add_argument('--fixed_pc_id', type=int, help='fixed point cloud id')
        parser.add_argument('--sampling_perc', type=float, help='subsampling percentage')
        parser.add_argument('--use_rand_e', action="store_true", help='use random e')
        parser.add_argument('--dim_e', type=int, default=1, help='dimension of variable')
        parser.add_argument('--masksize', type=float, default=0.9, help='the number of zeros in a mask')
        parser.add_argument('--mlp_layer_k', type=int, help='-1 for original xyz, 0 for first layer in mlp net')
        # parser.add_argument('--code_layer_k', type=int, help='-1 for after pooling, 0 for first layer in fc net')
        parser.add_argument('--sampling_fc_dims', type=int, nargs='+', default=[], help='pointnet fc dims')
        parser.add_argument('--not_share_weights', action="store_true", help='do not share weights, flatten inputs')

        parser.add_argument('--pretrained_weights', type=str, help='pretrained autoencoder or the whole model')
        # bsn layers
        parser.add_argument('--bsn_estimator', type=str, default='st',
                            help='binary stocastic neuron estimator, [st | reinforce]')
        parser.add_argument('--bsn_pass', action='store_false', help='pass through for st estimator')
        parser.add_argument('--bsn_sampling', action='store_true', help='sampling the prediction')
        parser.add_argument('--bsn_anneal_policy', type=str, default='exp', help='slope annealing policy')
        parser.add_argument('--bsn_anneal_rate', type=float, default=1.015, help='slope annealing rate')
        parser.add_argument('--bsn_anneal_step', type=int, default=50, help='slope annealing freqency')

        # for baseline
        parser.add_argument('--instance_file', default='data/modelnet40_ply_hdf5_2048/sample_instance.txt',
                            type=str, help='root to the file that contains instance ids')
        parser.add_argument('--subsmpl_file', default='data/modelnet40_ply_hdf5_2048/org_pc/original',
                            type=str, help='root to subsampled files')
        parser.add_argument('--subsmpl_file_suffix', default='_pect-0.05_msksize-0.9/optimal.npz',
                            type=str, help='suffix of subsampled files')
        parser.add_argument('--save_dir', default='data/modelnet40_ply_hdf5_2048/')
        parser.add_argument('--prefix', type=str, help='prefix for saving results')

        # regularization by difference between hidden vectors
        parser.add_argument('--regularize', action="store_true",
                            help='if regularize or not with difference between hidden vectors')
        parser.add_argument('--reg_weight', default=2.5, type=float,
                            help='regularization weight for difference between optimized and original code word')
        parser.add_argument('--code_word_path', default='./checkpoints/no-smpl-all/latest/preds_result.npz',
                            type=str, help='path to original')
        # parser.add_argument('--init_type', default='zeros', type=str, help='init type of e')

        return parser

    def initialize(self, opt, **kwargs):
        BaseModel.initialize(self, opt, **kwargs)
        self.loss_names += ['pointnet']
        self.n_points = self.opt.n_points + 1

    def custom_network(self, inputs, train_phase=True, annealing_slope=None, **kwargs):
        self.loss_over_budget = 0
        self.pc = inputs[1]
        if self.opt.sampling_perc is None:
            with tf.variable_scope('pointnet'):
                self.pointnet = PointNet(self.opt.pointnet_mlp_dims, self.opt.pointnet_fc_dims, is_train=train_phase)
                logits = self.pointnet(inputs[0])
                self.raw_input = inputs[0]
                self.logits = logits
                self.loss_entropy_vec = tf.nn.sparse_softmax_cross_entropy_with_logits(
                    labels=inputs[1], logits=logits)
                self.loss_pointnet = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
                    labels=inputs[1], logits=logits))
                trainable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='pointnet')
                self.loss_cost = self.loss_pointnet
                self.pred_pc = tf.nn.softmax(logits)
                self.loss_accu_all, self.loss_accu_batch = tf.metrics.accuracy(inputs[1], tf.argmax(self.pred_pc, -1))
                self.loss_names += ['accu_all', 'accu_batch']
        else:
            #self.mask = tf.placeholder(tf.float32, shape=(self.n_points - 1))
            #keep = tf.expand_dims(tf.concat((self.mask, tf.ones(1)), 0), 1)

            sample = self._sub_sampling(tf.reduce_sum(inputs[0], -1, keepdims=True))
            optim_var = self.__pred_binary_stocastic(sample, annealing_slope)
            self.optim_var = tf.concat((optim_var, tf.ones([1, 1])), 0)

            with tf.variable_scope('pointnet', reuse=tf.AUTO_REUSE):
                self.pointnet = PointNet(self.opt.pointnet_mlp_dims, self.opt.pointnet_fc_dims, is_train=train_phase)
                var_mask = tf.keras.layers.Dropout(rate=self.opt.masksize)(self.optim_var, training=True)
                X_mask = self.pointnet.mlp_net(inputs[0] * tf.expand_dims(var_mask, 0))
                X = self.pointnet.mlp_net(inputs[0] * tf.expand_dims(self.optim_var, 0))

            with tf.variable_scope('pointnet', reuse=tf.AUTO_REUSE):
                self.post_mlp_x = X
                self.selected_x = inputs[0] * tf.expand_dims(self.optim_var, 0)
                self.raw_input = inputs[0]
                self.code_word_mask = tf.reduce_max(X_mask, 1)
                self.code_word = tf.reduce_max(X, 1)
                logits_mask = self.pointnet._pool_and_predict(X_mask)
                logits = self.pointnet._pool_and_predict(X)
                self.logits = logits

            tfprint = tf.print(tf.reduce_sum(sample), tf.reduce_sum(self.optim_var))
            with tf.control_dependencies([tfprint]):
                overb = tf.reduce_sum(self.optim_var) / self.n_points - self.opt.sampling_perc
                self.loss_over_budget = overb

            tfprint_pred = tf.print(inputs[1], tf.shape(logits), tf.argmax(logits, 1))
            with tf.control_dependencies([tfprint_pred]):
                self.loss_pointnet2 = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
                    labels=inputs[1], logits=logits))
                self.loss_entropy_vec = tf.nn.sparse_softmax_cross_entropy_with_logits(
                    labels=inputs[1], logits=logits)
                self.loss_pointnet = tf.maximum(1.0, tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
                    labels=inputs[1], logits=logits_mask)))
            self.loss_names += ['over_budget', 'pointnet2']

            if self.opt.regularize:
                code_word_org = np.load(self.opt.code_word_path)['code_word'][self.opt.fixed_pc_id]
                regu_loss = tf.norm(tf.subtract(code_word_org, self.code_word), 2) / (tf.norm(
                    code_word_org, 2))
                regu_loss_mask = tf.norm(tf.subtract(code_word_org, self.code_word_mask), 2) / (tf.norm(
                    code_word_org, 2))

                self.loss_regu = self.opt.reg_weight * regu_loss
                self.loss_regu_mask = self.opt.reg_weight * regu_loss_mask
                self.loss_pointnet = self.loss_regu_mask
                self.loss_names += ['regu']

            trainable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='sampling')
            loss_over_budget = self.__add_contrained_optimizer(trainable)
            self.loss_cost = self.loss_pointnet + loss_over_budget

        return trainable

    def load_weights(self, sess, **kwargs):
        if self.opt.pretrained_weights is None:
            self.opt.pretrained_weights = self.save_dir + "/latest.ckpt"
        varlist = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='pointnet')
        loader = tf.train.Saver(varlist)
        if not os.path.exists(self.opt.pretrained_weights + '.meta'):
            print('---> {} not exist.'.format(self.opt.pretrained_weights))
            return False
        else:
            loader.restore(sess, self.opt.pretrained_weights)
            return True

    def check_result(self, sess, feed_dict, prefix=''):
        res_ops = [self.pc, self.pred_pc, self.pointnet.code,
                   [self.loss_accu_all, self.loss_accu_batch, self.loss_pointnet], self.raw_input, self.logits,
                   self.pointnet.post_mlp_x]
        try:
            values = sess.run(res_ops)
        except tf.errors.OutOfRangeError:
            return None
        # print('no. samples in one batch: {}'.format(values[1].shape[0]))
        return values
        # return np.vstack(labels), np.vstack(preds), np.vstack(prob)

    def __add_contrained_optimizer(self, trainable_var):
        self.com = ConstrainedOptimizationModel(beta0=self.opt.copt_beta, method="lagrange",
                                                bound_right=not self.opt.copt_beta_remove_upper_bound,
                                                clip_right=self.opt.copt_beta_clip_beta)
        loss_over_budget = self.com.adapt_budget_penalty(-self.loss_pointnet,
                                                         self.loss_over_budget, trainable_var)
        self.beta_cur, self.beta_left, self.beta_right = self.com.beta, self.com._left_bound, self.com._right_bound
        self.visual_ops += ['beta_cur', 'beta_left', 'beta_right']
        return loss_over_budget

    def __pred_binary_stocastic(self, pred, annealing_slope=None):
        pred = binary_stocastic_layer(pred, estimator=self.opt.bsn_estimator,
                                      stochastic_tensor=tf.constant(self.opt.bsn_sampling),
                                      pass_through=self.opt.bsn_pass, slope_tensor=annealing_slope)
        return pred

    def __prep_feats(self, X=None):
        feats = []
        if self.opt.use_rand_e:
            if self.opt.init_type == 'zeros':
                self.kns_product_e = tf.get_variable("product_e", shape=(self.n_points - 1, self.opt.dim_e,),
                                                     dtype=tf.float32, initializer=tf.zeros_initializer)
            elif self.opt.init_type == 'he_uniform':
                self.kns_product_e = tf.get_variable("product_e", shape=(self.n_points - 1, self.opt.dim_e,),
                                                     dtype=tf.float32,
                                                     initializer=tf.contrib.layers.variance_scaling_initializer(
                                                         uniform=True))
            elif self.opt.init_type == 'he_normal':
                self.kns_product_e = tf.get_variable("product_e", shape=(self.n_points - 1, self.opt.dim_e,),
                                                     dtype=tf.float32,
                                                     initializer=tf.contrib.layers.variance_scaling_initializer())
            elif self.opt.init_type == 'xavier_uniform':
                self.kns_product_e = tf.get_variable("product_e", shape=(self.n_points - 1, self.opt.dim_e,),
                                                     dtype=tf.float32, initializer=tf.initializers.glorot_uniform)
            elif self.opt.init_type == 'xavier_normal':
                self.kns_product_e = tf.get_variable("product_e", shape=(self.n_points - 1, self.opt.dim_e,),
                                                     dtype=tf.float32, initializer=tf.initializers.glorot_normal)
            elif self.opt.init_type == 'lecun_uniform':
                self.kns_product_e = tf.get_variable("product_e", shape=(self.n_points - 1, self.opt.dim_e,),
                                                     dtype=tf.float32,
                                                     initializer=tf.contrib.layers.variance_scaling_initializer(
                                                         factor=1., uniform=True))
            elif self.opt.init_type == 'lecun_normal':
                self.kns_product_e = tf.get_variable("product_e", shape=(self.n_points - 1, self.opt.dim_e,),
                                                 dtype=tf.float32,
                                                 initializer=tf.contrib.layers.variance_scaling_initializer(
                                                     factor=1.))
            else:
                raise NotImplementedError('init_type {} is not supported!'.format(self.opt.init_type))

            feats += [self.kns_product_e]

        if self.opt.mlp_layer_k is not None:
            if self.opt.mlp_layer_k == -1:
                feats += [X]
            elif self.opt.mlp_layer_k >= 0:
                feats += [self.pointnet.mlp_net.layers[self.opt.mlp_layer_k]]

        # code = None
        # if self.opt.code_layer_k > -2:
        #     code = self.pointnet.fc_net.code if self.opt.code_layer_k == -1 \
        #         else [self.pointnet.fc_net.layers[self.opt.code_layer_k]]

        if self.opt.not_share_weights:  # flatten the inputs
            feat = tf.concat(feats, -1)
            feat = tf.reshape(feat, (1, -1))
            # if code is not None:
            #     feat = tf.concat((feat, tf.reshape(code, (1, -1))), -1)
        else:
            # if code is not None:
            #     feats += [tf.tile(tf.reshape(code, (1, -1)), (self.n_points, 1))]
            feat = tf.concat(feats, -1)
        return feat

    def _sub_sampling(self, inputs, **kwargs):
        with tf.variable_scope('sampling'):
            self.samplingnet = MLPNet(self.opt.sampling_fc_dims, name='sampling')
            X = self.__prep_feats(inputs[0])
            return self.samplingnet(X, **kwargs)
