from .base_model import BaseModel
from .constrained_optim_model import ConstrainedOptimizationModel
from .binary_stocastic_neurons import binary_stocastic_layer
import tensorflow as tf
#import tensorflow_probability as tfp
import numpy as np
import warnings
import pandas as pd


class KnapsackModel(BaseModel):
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        # dataset
        parser.add_argument('--n_prods', type=int, default=10, help='number of products')
        # network input type
        parser.add_argument('--use_val_cost', action='store_true', help='use value cost and budget as input')
        parser.add_argument('--use_rand_e', action='store_true', help='use random vectors as input')
        parser.add_argument('--dim_e', type=int, default=4, help='dimension of variable')
        parser.add_argument('--use_budget', action='store_true', help='use budget as input')
        parser.add_argument('--use_shared_e', action='store_true', help='use random vectors as input')
        parser.add_argument('--dim_shared_e', type=int, default=4, help='dimension of variable')
        parser.add_argument('--masksize', type=float, default=0.9, help='the number of zeros in a mask')
        parser.add_argument('--budgetdivide', type=float, default=1, help='how to divide the input budget')

        # input config
        parser.add_argument('--min_val', type=float, default=0.0001, help='minimum value')
        parser.add_argument('--max_val', type=float, default=1.0, help='maximum value')
        parser.add_argument('--min_cost', type=float, default=0.0001, help='minimum cost')
        parser.add_argument('--max_cost', type=float, default=1.0, help='maximum cost')
        parser.add_argument('--min_budget', type=float, default=0.1, help='minimum budget')
        parser.add_argument('--max_budget', type=float, default=0.3, help='maximum budget')
        parser.add_argument('--rand_value', type=str, default="uniform", help='random number method for value')
        parser.add_argument('--rand_cost', type=str, default="uniform", help='random number method for cost')
        parser.add_argument('--rand_budget', type=str, default="uniform", help='random number method for budget')
        # network parameters
        parser.add_argument('--fc1_dims', type=int, nargs='+', default=None, help='number of features fc block1')
        parser.add_argument('--fc2_dims', type=int, nargs='+', default=None,
                            help='number of features fc block2')
        parser.add_argument('--activ', type=str,  default=None, help='activiation layer')
        parser.add_argument('--norm', type=str, default=None, help='normalization layer')
        parser.add_argument('--not_share_weights', action="store_true", help='do not share weights, flatten inputs')
        # for gumbel softmax
        # parser.add_argument('--init_temp', type=float, default=5.0, help='initial temperature')
        # parser.add_argument('--train_temp', action='store_true', help='train temperature')
        # for binary stocastic
        parser.add_argument('--bsn_estimator', type=str, default='st', help='binary stocastic neuron estimator')
        parser.add_argument('--bsn_pass', action='store_true', help='pass through for st estimator')
        parser.add_argument('--bsn_sampling', action='store_true', help='sampling the prediction')
        parser.add_argument('--bsn_anneal_policy', type=str, default='exp', help='slope annealing policy')
        parser.add_argument('--bsn_anneal_rate', type=float, default=1.01, help='slope annealing rate')
        parser.add_argument('--bsn_anneal_step', type=int, default=1, help='slope annealing step')
        # baseline
        parser.add_argument('--baseline_methods', type=str, nargs='+', default=['gr'], help='baseline methods')

        return parser

    def __init__(self, **kwargs):
        super(KnapsackModel, self).__init__(**kwargs)

    def initialize(self, opt, **kwargs):
        BaseModel.initialize(self, opt, **kwargs)
        self.__eye = tf.eye(opt.n_prods, dtype=tf.float32)
        # if "gumbel" in self.opt.model:
        #     self.temperature = tf.Variable(5, trainable=opt.train_temp)
        self.over_budget_penalty = tf.Variable(0.0, trainable=False)
        self.loss_names += ['value', 'over_budget', 'value2']

        assert self.opt.use_val_cost + self.opt.use_rand_e > 0, "must have at least one input!"

        # load data
        if self.opt.data_root is None:  # generate data
            self.__gen_input()
        else:
            self.values_np, self.costs_np, self.budget = load_pisinger(self.opt.dataroot)
            self.budget = self.budget / opt.budgetdivide
            self.opt.n_prods = len(self.values_np)
        self.total_value, self.total_cost = np.sum(self.values_np), np.sum(self.costs_np)
        print("total value {}, total_cost {},  budget {}".format(self.total_value, self.total_cost, self.budget))

    def run_baseline_methods(self, log_file):
        with open(log_file, 'a') as logfile:
            message = "Total value, total cost, budget: {} {} {}\n".format(
                np.sum(self.values_np), np.sum(self.costs_np), self.budget)
            logfile.write(message)

            baseline_res = {}
            for ms in self.opt.baseline_methods:
                baseline_res[ms] = self.baseline(ms)
                message = "Baseline {}: {}\n".format(ms, baseline_res[ms])
                print(message)
                logfile.write(message)

    def custom_network(self, inputs=None, train_phase=True, annealing_slope=None, **kwargs):
        feat = self.__prep_feats()
        #mask = tf.placeholder(tf.float32, shape=(self.opt.n_prods))
        if self.opt.fc1_dims is not None:
            # skip last activation of fc block1 only when shared weights and has a second fc block
            skip_last = not self.opt.not_share_weights and self.opt.fc2_dims is not None
            pre_pred = self.get_mlp_model(feat, self.opt.fc1_dims, self.opt.activ, self.opt.norm, skip_last, train_phase)
            if not self.opt.not_share_weights and self.opt.fc2_dims is not None:
                embed = tf.tile(tf.expand_dims(tf.reduce_mean(pre_pred, 0), 0), (self.opt.n_prods, 1))
                feat = tf.concat((pre_pred, embed), -1)
                pre_pred = self.get_mlp_model(feat, self.opt.fc1_dims, self.opt.activ, self.opt.norm, False, train_phase)
            elif self.opt.not_share_weights and self.opt.fc2_dims is not None:
                warnings.warn("No shared weights, fc2_dim is ignored", SyntaxWarning)
            # add the last layer to produce the result
            lastdim = self.opt.n_prods if self.opt.not_share_weights else 1
            pred = tf.reshape(tf.keras.layers.Dense(lastdim, use_bias=False)(pre_pred), (-1,))
            # add binary stocastic layer
            pred = self.__pred_binary_stocastic(pred)
        else:
            pred = self.__pred_binary_stocastic(feat)
        # derive the loss
        self.__custom_loss(pred)

    def __pred_binary_stocastic(self, pred, annealing_slope=None):
        pred = binary_stocastic_layer(pred,  estimator=self.opt.bsn_estimator,
                                      stochastic_tensor=tf.constant(self.opt.bsn_sampling),
                                      pass_through=self.opt.bsn_pass, slope_tensor=annealing_slope)
        return pred

    def __gen_input(self):
        self.values_np = np.random.uniform(self.opt.min_val, self.opt.max_val, self.opt.n_prods)
        self.costs_np = np.random.uniform(self.opt.min_cost, self.opt.max_cost, self.opt.n_prods)
        self.budget = np.random.uniform(self.opt.min_budget, self.opt.max_budget) * self.opt.n_prods
        np.savez(self.save_dir+"//data.npz", v=self.values_np, c=self.costs_np, b=self.budget)

    def __load_instance(self):
        pass

    def __prep_feats(self):
        self.values = tf.constant(self.values_np, dtype=tf.float32)
        self.costs = tf.constant(self.costs_np, dtype=tf.float32)
        feats = []
        if self.opt.use_rand_e:
            self.kns_product_e = tf.get_variable("product_e", shape=(self.opt.n_prods, self.opt.dim_e,),
                                         dtype=tf.float32, initializer=tf.zeros_initializer)
            feats += [self.kns_product_e]
        if self.opt.use_shared_e:
            self.kns_problem_e = tf.get_variable("kproblem_e", shape=(self.opt.dim_shared_e,),
                                         dtype=tf.float32, initializer=tf.zeros_initializer)

        if self.opt.use_val_cost:
            val_cost = tf.stack((self.values, self.costs), -1)
            feats += [val_cost]

        if self.opt.not_share_weights:  # flatten the inputs
            feat = tf.concat(feats, -1)
            feat = tf.reshape(feat, (1, -1))
            if self.opt.use_budget:
                feat = tf.concat((feat, tf.ones((1, 1))*self.budget), -1)
            if self.opt.use_shared_e:
                feat = tf.concat((feat, tf.reshape(self.kns_problem_e, (1, -1))), -1)
        else:
            if self.opt.use_budget:
                feats += [tf.ones((self.opt.n_prods, 1)) * self.budget]
            if self.opt.use_shared_e:
                feats += [tf.tile(tf.reshape(self.kns_problem_e, (1, -1)), (self.opt.n_prods, 1))]

            feat = tf.concat(feats, -1)

        return feat

    def __custom_loss(self, pred):
        self.optim_var = tf.reshape(pred, (-1,))
        optm_var_mask = tf.keras.layers.Dropout(rate=self.opt.masksize)(self.optim_var, training=True)
        # tfprint = tf.print(tf.reduce_sum(pred), tf.reduce_max(self.costs), self.costs * pred)
        # with tf.control_dependencies([tfprint]):
        self.loss_value2 = tf.reduce_sum(self.values * self.optim_var)
        self.loss_value = tf.reduce_sum(self.values * optm_var_mask)
        self.loss_over_budget = (tf.reduce_sum(self.costs * self.optim_var) - self.budget)
        self.loss_value_normalized = self.loss_value / self.total_value
        self.loss_over_budget_normalized = self.loss_over_budget / self.budget
        loss_over_budget = self.__add_contrained_optimizer()
        self.loss_cost = -self.loss_value_normalized + loss_over_budget


    def __add_contrained_optimizer(self):
        self.com = ConstrainedOptimizationModel(beta0=self.opt.copt_beta, method="lagrange",
                                                bound_right=not self.opt.copt_beta_remove_upper_bound,
                                                clip_right=self.opt.copt_beta_clip_beta)
        loss_over_budget = self.com.adapt_budget_penalty(self.loss_value_normalized,
                                                         self.loss_over_budget_normalized, None)
        self.beta_cur, self.beta_left, self.beta_right = self.com.beta, self.com._left_bound, self.com._right_bound
        self.visual_ops += ['beta_cur', 'beta_left', 'beta_right']
        return loss_over_budget

    def baseline(self, method='gr'):
        if method == "dp":
            return self.__baseline_dp()
        elif method == "gr":
            return self.__baseline_gr()
        else:
            raise NotImplementedError("Baseline method {} not implemented!".format(method))

    def __baseline_dp(self):
        def knapsack_dp(W, wt, val):
            # inputs must be integers
            K = [[0 for x in range(W + 1)] for x in range(n + 1)]

            # Build table K[][] in bottom up manner
            for i in range(len(val) + 1):
                for w in range(W + 1):
                    if i == 0 or w == 0:
                        K[i][w] = 0
                    elif wt[i - 1] <= w:
                        K[i][w] = max(val[i - 1] + K[i - 1][w - wt[i - 1]], K[i - 1][w])
                    else:
                        K[i][w] = K[i - 1][w]

            return K[len(val)][W]

        return knapsack_dp(self.budget, self.costs_np, self.values_np)

    def __baseline_gr(self):
        def knapsack_gr(ws, vs, b):
            tw, tv = 0.0, 0.0
            for i in range(len(vs)):
                if tw + ws[i] < b:
                    tv += vs[i]
                    tw += ws[i]
            return tv

        xx = self.values_np / self.costs_np
        idx = np.argsort(-xx)
        w, v = self.costs_np[idx], self.values_np[idx]

        return knapsack_gr(w, v, self.budget)


def random_func(shape, method='uniform', para1=None, para2=None):
    if method == "uniform":
        return tf.random_uniform(shape, para1, para2)
    elif method == "normal":
        return tf.random_normal(shape, para1, para2)
    elif method == "gamma":
        return tf.random_gamma(shape, para1, para2)
    else:
        raise NotImplementedError("Method {} not implemented!".format(method))

def load_pisinger(filename):
    df = pd.read_csv(filename, header=-1)
    N, budget = int(df.iloc[0, 0]), df.iloc[-1, 0]
    values, costs = df.iloc[1:-1, 2].values, df.iloc[1:-1, 3].values
    print(type(values), type(costs), type(budget))
    return values, costs, budget