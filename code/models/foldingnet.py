import numpy as np
import tensorflow as tf
from .basic_nets import BaseNet, PointNet, FoldingNetSingle, MLPNet


class FoldingNetBase(BaseNet):
    def __init__(self, opt, **kwargs):
        super(FoldingNetBase, self).__init__(**kwargs)
        self.opt = opt
        self.grid_dims = opt.grid_dims
        self._Npts = self.grid_dims[0] * self.grid_dims[1]
        self.point_dim, self.manifold_dim = 3, 2
        self.encoder_net = PointNet(opt.pointnet_mlp_dims, opt.pointnet_fc_dims, **kwargs)
        self._make_folding_nets(opt.fold1_dims, opt.fold2_dims, opt.fold1_kernel_size, opt.fold2_kernel_size)

        self.atlas_net = None
        if opt.atlas_num is not None:
            assert opt.atlas_kernels[-1] % opt.atlas_num == 0, \
                "Last dimension of atlas_kernel has to be dividable by num_atlas!"
            self.atlas_num, self.atlas_code_dim = opt.atlas_num, opt.atlas_kernels[-1] // opt.atlas_num
            self.atlas_net = MLPNet(opt.atlas_kernels, **kwargs)

        self.code, self.pred_X = None, None

    def call(self, input, **kwargs):
        self.code = self.encode(input, **kwargs)
        if self.atlas_net is not None:
            self.code = tf.reshape(self.atlas_net(self.code), (-1, self.atlas_code_dim))
        if "n_interp" in kwargs.keys():
            self.interpolate(kwargs['n_interp'])

        self.gen_grid()
        self.pred_X = self.decode(code=self.code, **kwargs)
        return self.pred_X

    def gen_grid(self):
        grid = tf.expand_dims(tf.constant(gen_grid_coords(self.grid_dims[0], self.grid_dims[1])), 0)
        self.grid2d = tf.tile(grid, tf.concat([tf.shape(self.code)[0:1], tf.constant([1, 1, 1])], 0))

    def encode(self, input, **kwargs):
        code = self.encoder_net(input, **kwargs)
        return code

    def decode(self, **kwargs):
        pass

    def interpolate(self, n_interp):
        delta = 1. / (n_interp + 1)  # n interpolation points => n+1 sections
        ratio = delta * (tf.reshape(tf.range(n_interp, dtype=tf.float32), (-1, 1)) + 1)
        code = tf.tile(self.code, (n_interp, 1))
        self.code = code[0] * (1. - ratio) + code[1] * ratio

    def flatten(self, input):
        if self.atlas_net is not None:
            return tf.reshape(input, (-1, self.atlas_num * self._Npts, self.point_dim))
        else:
            return tf.reshape(input, (-1, self._Npts, self.point_dim))

    def _make_folding_nets(self, fold1_dims=None, fold2_dims=None,
                           fold1_kernel_size=None, fold2_kernel_size=None, **kwargs):
        self.fold1_net, self.fold2_net = None, None
        if fold1_dims is not None:
            self.fold1_net = FoldingNetSingle(fold1_dims, fold1_kernel_size, **kwargs)
        if fold2_dims is not None:
            self.fold2_net = FoldingNetSingle(fold2_dims, fold2_kernel_size, **kwargs)


class FoldingNet(FoldingNetBase):
    def __init__(self, opt, **kwargs):
        super(FoldingNet, self).__init__(opt, **kwargs)

    def decode(self, code, **kwargs):
        code = tf.expand_dims(tf.expand_dims(code, 1), 1)
        code = tf.tile(code, (1, self.grid_dims[1], self.grid_dims[0], 1))  # B x M x N x N x k

        X = tf.concat([code, self.grid2d], -1)
        X = self.fold1_net(X, **kwargs)
        X = tf.concat([code, X], -1)
        X = self.fold2_net(X, **kwargs)

        return X


def gen_grid_coords(nx, ny, flatten=False):
    """
    Generate grid, inner x (-1 --> 1) major, y (-1 --> 1)
    :param nx number of points on x axe
    :param ny number of points on y axe
    :return [x, y]
    """
    range_x = np.linspace(-1.0, 1.0, nx)
    range_y = np.linspace(-1.0, 1.0, ny)
    xv, yv = np.meshgrid(range_x, range_y)
    if flatten:
        xv, yv = xv.flatten(), yv.flatten()
    grid2d = np.stack([xv, yv], axis=-1)
    return grid2d.astype(np.float32)


