from .base_options import BaseOptions


class TrainOptions(BaseOptions):
    def initialize(self, parser):
        parser = BaseOptions.initialize(self, parser)
        parser.add_argument('--phase', type=str, default='train', help='train, val, test, etc')

        # print/display/checkpoints
        parser.add_argument('--display_freq', type=int, default=10, help='frequency of showing training results on screen')
        parser.add_argument('--display_ncols', type=int, default=0, help='if positive, display all images in a single visdom web panel with certain number of images per row.')
        parser.add_argument('--update_html_freq', type=int, default=2000, help='frequency of saving training results to html')
        parser.add_argument('--print_freq', type=int, default=1, help='frequency of showing training results on console')
        parser.add_argument('--save_epoch_freq', type=int, default=5, help='frequency of saving checkpoints at the end of epochs')
        parser.add_argument('--test_epoch_freq', type=int, default=5, help='frequency of testing')
        parser.add_argument('--continue_train', action='store_true', help='continue training: load the latest model')
        parser.add_argument('--epoch_count', type=int, default=1, help='the starting epoch count, we save the model by <epoch_count>, <epoch_count>+<save_latest_freq>, ...')
        parser.add_argument('--which_epoch', type=str, default='latest', help='which epoch to load? set to latest to use latest cached model')
        parser.add_argument('--n_epoch', type=int, default=1000, help='number of epochs to train')
        parser.add_argument('--n_batches', type=int, help='number of batches in the dataset')
        parser.add_argument('--drop_ratio', type=float, default=0.0, help='dropout ratio of dropout layers')
        parser.add_argument('--niter', type=int, default=100, help='# of iter at starting learning rate')
        parser.add_argument('--niter_decay', type=int, default=100, help='# of iter to linearly decay learning rate to zero')
        parser.add_argument('--pool_size', type=int, default=0, help='the size of image buffer that stores previously generated images')
        parser.add_argument('--no_html', action='store_true', help='do not save intermediate training results to [opt.checkpoints_dir]/[opt.name]/web/')

        # optimizer
        parser.add_argument('--beta1', type=float, default=0.5, help='momentum term of adam')
        parser.add_argument('--lr', type=float, default=0.0002, help='initial learning rate for adam')
        parser.add_argument('--lr_decay_policy', type=str, default='fixed', help='learning rate policy: fixed|exp')
        parser.add_argument('--lr_decay_step', type=int, default=50, help='decay step')
        parser.add_argument('--lr_decay_rate', type=float, default=0.96, help='decay rate')

        # constrained optimization parameters
        parser.add_argument('--copt_beta', type=float, default=1.0, help='marginal penalty')
        parser.add_argument('--copt_beta_remove_upper_bound', action='store_true',
                            help="do not use upper bound, use beta0 * overrun + lower_bound")
        parser.add_argument('--copt_beta_clip_beta', action='store_true',
                            help='use beta0 * overrun + lower_bound to clip beta')
        parser.add_argument('--total_tolerance', type=int, default=500, help='total tolerance')


        self.isTrain = True
        return parser