import argparse
import os
from util import util
import models

class BaseOptions():
    def __init__(self):
        self.initialized = False

    def initialize(self, parser):
        parser.add_argument('--name', type=str, default='experiment_name', help='name of the experiment. It decides where to store samples and models')
        parser.add_argument('--suffix', default='', type=str, help='customized suffix: opt.name = opt.name + suffix: e.g., {model}_{netG}_size{loadSize}')

        # model
        parser.add_argument('--model', type=str, default='knapsack', help='chooses which model to use. ego_auto, ego_decoder')
        parser.add_argument('--n_blocks', type=int, default=5, help='# of conv/deconv blocks')
        parser.add_argument('--init_type', type=str, default='lecun_normal',
                            help='network initialization [normal|xavier|kaiming|orthogonal]')

        # data
        parser.add_argument('--data_root',
                            help='path to images (should have subfolders trainA, trainB, valA, valB, etc)')
        parser.add_argument('--dataset_name', help='dataset name')
        parser.add_argument('--augment_data', action='store_true', help='augment the dataset')
        parser.add_argument('--batch_size', type=int, default=32, help='input batch size')

        # display/checkpoints
        parser.add_argument('--display_winsize', type=int, default=512, help='display window size')
        parser.add_argument('--display_id', type=int, default=1, help='window id of the web display')
        parser.add_argument('--display_server', type=str, default="http://localhost", help='visdom server of the web display')
        parser.add_argument('--display_port', type=int, default=8099, help='visdom port of the web display')
        parser.add_argument('--verbose', action='store_true', help='if specified, print more debugging information')
        parser.add_argument('--checkpoints_dir', type=str, default='./checkpoints', help='models are saved here')

        # misc
        parser.add_argument('--gpu_ids', type=str, default='0', help='gpu ids: e.g. 0  0,1,2, 0,2. use -1 for CPU')
        parser.add_argument('--nThreads', default=4, type=int, help='# threads for loading data')

        self.initialized = True
        return parser

    def gather_options(self):

        # initialize parser with basic options
        if not self.initialized:
            parser = argparse.ArgumentParser(
                formatter_class=argparse.ArgumentDefaultsHelpFormatter)
            parser = self.initialize(parser)

        # get the basic options
        opt, unknown = parser.parse_known_args()

        # modify model-related parser options
        model_name = opt.model
        model_option_setter = models.get_option_setter(model_name)
        parser = model_option_setter(parser, self.isTrain)

        # POSSIBLE FEATURE:
        # modify dataset-related parser options

        self.parser = parser

        return parser.parse_args()

    def print_options(self, opt):
        message = ''
        message += '----------------- Options ---------------\n'
        for k, v in sorted(vars(opt).items()):
            comment = ''
            default = self.parser.get_default(k)
            if v != default:
                comment = '\t[default: %s]' % str(default)
            message += '{:>25}: {:<30}{}\n'.format(str(k), str(v), comment)
        message += '----------------- End -------------------'
        print(message)

        # save to the disk
        save_dir = os.path.join(opt.checkpoints_dir, opt.name)
        util.mkdirs(save_dir)
        file_name = os.path.join(save_dir, 'opt.txt')
        with open(file_name, 'wt') as opt_file:
            opt_file.write(message)
            opt_file.write('\n')

    def parse(self):

        opt = self.gather_options()
        opt.isTrain = self.isTrain   # train or test

        # process opt.suffix
        if opt.suffix:
            suffix = ('_' + opt.suffix.format(**vars(opt))) if opt.suffix != '' else ''
            suffix = suffix.replace(', ', '+')
            suffix = suffix.replace('[', '')
            suffix = suffix.replace(']', '')
            opt.name = opt.name + suffix

        self.print_options(opt)

        # set gpu ids
        str_ids = opt.gpu_ids.split(',')
        opt.gpu_ids = []
        for str_id in str_ids:
            id = int(str_id)
            if id >= 0:
                opt.gpu_ids.append(id)
        if len(opt.gpu_ids) > 0:
            # set the gpu, need revision
            pass

        self.opt = opt
        return self.opt
