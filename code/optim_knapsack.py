
from options.train_options import TrainOptions
from models import create_model
from util.gpulock import GPULock
from util.util import print_options

import tensorflow as tf
import numpy as np
import time, os

def train(opt, **kwargs):

    model = create_model(opt)

    global_step = tf.Variable(0, trainable=False, name='global_step')

    # set up annealing policy here
    annealing_slope = get_annealing_slope(opt, global_step=global_step)

    # define network
    model.custom_network(annealing_slope=annealing_slope)
    cost_ops = model.get_costs_ops()

    # define learning rate
    learning_rate = get_learning_rate(opt, global_step=global_step)

    # define training operator
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    with tf.control_dependencies(update_ops):
        # create train operations
        train_op = tf.train.GradientDescentOptimizer(learning_rate).minimize(model.loss_cost, gate_gradients=tf.train.GradientDescentOptimizer.GATE_GRAPH, global_step=global_step)

    # config log settings
    #saver = tf.train.Saver(model.get_save_varlist(), max_to_keep=opt.n_epoch)

    log_filename = os.path.join(model.save_dir, "log_loss.txt")
    print_options(log_filename, opt)

    # run baseline methods
    model.run_baseline_methods(log_filename)

    # add virualization ops
    vis_ops = [annealing_slope, learning_rate] + model.get_vis_ops()

    # start training
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())

        # best_result [loss, influence, over_budget, time]
        total_time, best_value, best_x = 0.0, 0.0, np.zeros((opt.n_prods,))
        total_tolerance = opt.total_tolerance
        for epoch in range(opt.n_epoch):
            if total_tolerance < 0:
                break
            #  add training code
            # mask = np.random.choice([0., 1.], size=(opt.n_prods,), p=[opt.masksize, 1.0-opt.masksize])
            # print("mask sum:", np.sum(mask))
            start_time = time.time()
            values = sess.run([cost_ops, model.optim_var, vis_ops, train_op])
            end_time = time.time()
            total_time += end_time - start_time


            if values[0][2] < 0 and values[0][3] > best_value:
                best_value = values[0][3]
                best_x = values[1]
                total_tolerance = opt.total_tolerance
            else:
                total_tolerance -= 1

            message = "epoch :{}, best: {}, {} : {}, {} : {}, time: {:.4f}".format(
                epoch + 1,  best_value, model.loss_names, values[0],
                ['slope', 'lr'] + model.visual_ops, values[2], total_time)
            print(message)

            if (epoch + 1) % opt.print_freq == 0:
                message += "\n"
                with open(log_filename, 'a') as logfile:
                    logfile.write(message)

        np.savez(os.path.join(model.save_dir, 'optimal.npz'), x=best_x, v=best_value)
        # save_path = saver.save(sess, os.path.join(model.save_dir, "latest.ckpt"))
        # print('Latest model saved at (epoch {:d}, total_steps {:d}) to {}'.format(epoch+1, opt.n_epoch, save_path))


def get_annealing_slope(opt, **kwargs):
    annealing_slope = tf.constant(1.0)
    if opt.bsn_estimator == 'st' and not opt.bsn_pass:
        annealing_slope = decay_variable(1.0, opt.bsn_anneal_policy, rate=opt.bsn_anneal_rate,
                                         step=opt.bsn_anneal_step, **kwargs)
    return annealing_slope


def get_learning_rate(opt, **kwargs):
    lr_rate = decay_variable(opt.lr, opt.lr_decay_policy, rate=opt.lr_decay_rate,
                             step=opt.lr_decay_step, **kwargs)
    return lr_rate


def decay_variable(initial_value, decay_policy='fixed', name=None, **kwargs):
    if decay_policy == 'exp':
        decayed = tf.train.exponential_decay(initial_value, kwargs['global_step'],
                                             kwargs['step'], kwargs['rate'], staircase=False)
    elif decay_policy == 'custom':
        decayed = tf.Variable(initial_value, dtype=tf.float32, trainable=False,
                              name=name, collections=[tf.GraphKeys.UPDATE_OPS])
    else:
        decayed = tf.constant(initial_value)

    return decayed


if __name__ == '__main__':
    #gpulock = GPULock()
    #gpulock.acquire()

    opt = TrainOptions().parse()

    train(opt)

    #gpulock.release()
