from options.train_options import TrainOptions
from data.pc_dataset import PointCloudDataset
from models import create_model
import models.train_util as train_util
import os
# from util.gpulock import GPULock
# os.environ["CUDA_VISIBLE_DEVICES"]="-1"


def train(opt):
    # prepare data
    train_dataset, test_dataset, nbatches = prepare_data(opt)
    print("No. of batches: {}".format(nbatches))

    opt.n_batches = nbatches
    # initialize models
    model = create_model(opt)

    if opt.phase == "train":
        train_util.train(opt, model, train_dataset, test_dataset)
    else:
        train_util.inference(opt, model, test_dataset)


def prepare_data(opt):
    if opt.phase == 'train':
        return prepare_data_train(opt)
    elif opt.phase == 'test':
        return prepare_data_test(opt)
    else:
        return None, None, None


def prepare_data_train(opt):
    train_data_loader = PointCloudDataset(opt.data_root, opt.dataset_name, opt.batch_size,
                                          opt.classes, augment=opt.augment_data)
    train_dataset = train_data_loader.create_dataset()
    train_dataset = train_dataset.repeat().batch(opt.batch_size)

    test_dataset = None
    try:
        test_batch_size = 1
        test_data_loader = PointCloudDataset(opt.data_root.replace('train', 'test'), opt.dataset_name,
                                             test_batch_size, opt.classes, augment=opt.augment_data, shuffle=False)
        test_dataset = test_data_loader.create_dataset()
        test_dataset = test_dataset.batch(test_batch_size, drop_remainder=True)
    except:
        print("Load test dataset from {} failed!".format(opt.data_root.replace('train', 'test')))
    return train_dataset, test_dataset, train_data_loader.n_batches


def prepare_data_test(opt):
    test_data_loader = PointCloudDataset(opt.data_root, opt.dataset_name, opt.batch_size,
                                          opt.classes, augment=opt.augment_data, shuffle=False)
    test_dataset = test_data_loader.create_dataset(opt.instance_file, opt.subsmpl_file, opt.subsmpl_file_suffix)
    test_dataset = test_dataset.repeat().batch(opt.batch_size)
    return None, test_dataset, test_data_loader.n_batches


if __name__ == '__main__':
    # gpu_lock = GPULock()
    # gpu_lock.acquire()

    opt = TrainOptions().parse()

    train(opt)

    # gpu_lock.release()