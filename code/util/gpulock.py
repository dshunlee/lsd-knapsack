import os
import time
import errno
import GPUtil

class GPULockException(Exception):
    pass


class GPULock(object):
    """ A file locking mechanism that has context-manager support so
        you can use it in a with statement. This should be relatively cross
        compatible as it doesn't rely on msvcrt or fcntl for the locking.
    """

    def __init__(self, maxLoad=0.5, maxMemory=0.5, excludeID=[], timeout=None, delay=.05):
        """ Prepare the file locker. Specify the file to lock and optionally
            the maximum timeout and the delay between each attempt to lock.
        """
        self.maxLoad, self.maxMemory, self.excludeID = maxLoad, maxMemory, excludeID
        
        self.lock_dir = os.path.join(os.path.expanduser('~'),'.gpu_locks')
        if not os.path.exists(self.lock_dir):
            os.makedirs(self.lock_dir)

        if timeout is not None and delay is None:
            raise ValueError("If timeout is not None, then delay must not be None.")
        self.is_locked = False
        self.timeout = timeout
        self.delay = delay

    def acquire(self):
        """ Acquire the lock, if possible. If the lock is in use, it check again
            every `wait` seconds. It does this until it either gets the lock or
            exceeds `timeout` number of seconds, in which case it throws
            an exception.
        """
        start_time = time.time()
        while True:
            try:
                if self._try_grab_gpu():
                    self.fd = os.open(self.lockfile, os.O_CREAT|os.O_EXCL|os.O_RDWR)
                    os.environ["CUDA_VISIBLE_DEVICES"] = "{}".format(self.gpu_id)
                    self.is_locked = True #moved to ensure tag only when locked
                    return self.gpu_id
                else:
                    if self.timeout is None:
                        pass
                    elif (time.time() - start_time) >= self.timeout:
                        raise GPULockException("Timeout occured.")
                    time.sleep(self.delay)

            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise
                if self.timeout is None:
                    pass
                    #raise GPULockException("Could not acquire lock on GPU {}".format(self.gpu_id))
                elif (time.time() - start_time) >= self.timeout:
                    raise GPULockException("Timeout occured.")
                time.sleep(self.delay)
        
        return None

    def release(self):
        """ Get rid of the lock by deleting the lockfile.
            When working in a `with` statement, this gets automatically
            called at the end.
        """
        if self.is_locked:
            os.close(self.fd)
            os.unlink(self.lockfile)
            self.is_locked = False

    def _try_grab_gpu(self):
        gpus = GPUtil.getAvailable(limit=1, maxLoad=self.maxLoad, maxMemory=self.maxMemory,
                excludeID=self.excludeID, order='random')
        if len(gpus)==0:
            return False
        else:
            self.gpu_id = gpus[0]
            self.lockfile = '{}/gpu_{}.lock'.format(self.lock_dir, self.gpu_id)
            return True

    def __enter__(self):
        """ Activated when used in the with statement.
            Should automatically acquire a lock to be used in the with block.
        """
        if not self.is_locked:
            self.acquire()
        return self


    def __exit__(self, type, value, traceback):
        """ Activated at the end of the with statement.
            It automatically releases the lock if it isn't locked.
        """
        if self.is_locked:
            self.release()


    def __del__(self):
        """ Make sure that the FileLock instance doesn't leave a lockfile
            lying around.
        """
        self.release()
