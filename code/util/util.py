import os


def mkdirs(paths):
    if isinstance(paths, list) and not isinstance(paths, str):
        for path in paths:
            mkdir(path)
    else:
        mkdir(paths)


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def print_options(log_name, opt):
    message = ''
    message += '\n\n================== Options ==================\n'
    for k, v in sorted(vars(opt).items()):
        comment = ''
        message += '{:>25}: {:<30}{}\n'.format(str(k), str(v), comment)
    message += '----------------- End -------------------'

    # save to the disk
    with open(log_name, 'w+') as opt_file:
        opt_file.write(message)
        opt_file.write('\n')
