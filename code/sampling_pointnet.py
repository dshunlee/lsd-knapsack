import models.optim_util as optim_util
from data.pc_dataset import PointCloudDataset
from models import create_model
from options.train_options import TrainOptions


def train(opt):
    # prepare data
    train_dataset = prepare_data(opt)

    opt.n_batches = 1
    # initialize models
    model = create_model(opt)

    optim_util.train(opt, model, train_dataset)


def prepare_data(opt):
    train_data_loader = PointCloudDataset(opt.data_root, opt.dataset_name, opt.batch_size,
                                          opt.classes, fixed_pc_id=opt.fixed_pc_id)
    train_dataset = train_data_loader.create_dataset()
    train_dataset = train_dataset.repeat().batch(opt.batch_size)

    return train_dataset


if __name__ == '__main__':
    # gpu_lock = GPULock()
    # gpu_lock.acquire()

    opt = TrainOptions().parse()

    opt.n_prods = opt.n_points

    train(opt)

    # gpu_lock.release()