#!/bin/bash

for data in data/Pisinger/small-coefficients/type*.csv data/Pisinger/large-coefficients/type*.csv
do
	file=${data##*/}
	file="${file%.*}"
	echo $file

    for budgetdivide in 1 10 100
    do
        for mask in 0.5 0.7 0.9
        do
            for lr in 1 10
            do
                echo lr, mask
                python code/optim_knapsack.py --name ${file}_${i} --dataroot ${data} --use_rand_e --dim_e 1 --n_epoch 10000 --total_tolerance 10000 --lr ${lr} --copt_beta 10 --masksize ${mask} --budgetdivide ${budgetdivide} > ${file}.log.${lr}.fc1_${mask}_${budgetdivide}
                python code/optim_knapsack.py --name ${file}_${i} --dataroot ${data} --use_rand_e --dim_e 1 --n_epoch 10000 --total_tolerance 10000 --lr ${lr} --copt_beta 1 --masksize ${mask} --budgetdivide ${budgetdivide} > ${file}.log.${lr}.nofc_${mask}_${budgetdivide}
            done
         done
    done
done
